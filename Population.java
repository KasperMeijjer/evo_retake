
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.vu.contest.ContestEvaluation;


public class Population {
	ArrayList<Candidate> list;
	double averageFitness;
	double stdDev;
	Random rnd_;

	public Population(Random rnd_){
		list = new ArrayList<Candidate>();
		this.rnd_ = rnd_;
	}
	
	public Population(ArrayList<Candidate> list, Random rnd_){
		this.list = list;
		this.rnd_ = rnd_;
	}
	
	public void initSigma(ArrayList<Double> init){
		for(int i = 0; i < list.size();i++){
//			System.out.println(init);
			list.get(i).sigmaList = init;
			
		}
		
	}
	
	public void merge(Population population){
		this.list.addAll(population.list);
	}
	
	public void initPopulation(int PopulationSize){
		for(int i = 0; i < PopulationSize; i++){
			Candidate candidate = new Candidate(true, rnd_);
			list.add(candidate);
		}
	}

	public void sortParentPopulation(){
		Collections.sort(list, Candidate.parSorFit);
	}
	
	public void setFitness(ContestEvaluation evaluation){
		double fitness = 0;
		for(Candidate c :list){
				 fitness = (double) evaluation.evaluate(c.genotype);
				 c.fitness = fitness;
		}
	}
	

//	public void setRank(){
//		for(int i = 0; i < list.size();i++){
//			Candidate c = list.get(i);
//			c.rank = list.size()-1-i;
//		}
//	}
//	
	public void setRandomOrder(){
		Collections.shuffle(list, rnd_);
	}
	
	public void setLinearRankingProb(double sel_pres){
		for(int i = 0; i < list.size();i++){
			Candidate c = list.get(i);
			int rank = list.size()-1-i;
			float constant = (float) ((2-sel_pres)/list.size());
			float variable = (float) (((2*rank)*(sel_pres-1))/(list.size()*(list.size()-1)));
			c.linRankProb = constant + variable;
			//System.out.println(c.linRankProb);
		}
	}
	
	
	//Er werd hier geen variantie aangemaakt, we gebruiken steeds de else optie. 
	public double setSigmaScale(double sigmaScale){
		double sumSigmaScale = 0.0;
		double mean = getMean();
		stdDev = getStdDev();
		
		for(int i = 0; i < list.size(); i++){
			Candidate c = list.get(i);
			
			if(stdDev != 0.0){
				c.sigmaScale = (float) Math.max(c.fitness - (mean- 2.0*stdDev),0.01);
				//System.out.println(c.sigmaScale);
				//c.sigmaScale = (float) (sigmaScale + (c.fitness - averageFitness)/2.0*variance);
			}else{
				c.sigmaScale = (float) sigmaScale;
			}
			sumSigmaScale += c.sigmaScale;
		}
		
		return sumSigmaScale;
	}	

		public double getMean(){
			double sum = 0.0;
			for(int i = 0; i < list.size(); i++){
				sum += list.get(i).fitness;
			}
			return sum/list.size();
		}
		
		public double getVariance(){
			double mean = getMean();
			double temp = 0;
			for(int i = 0; i< list.size(); i++){
				temp += (list.get(i).fitness-mean)*(list.get(i).fitness-mean);
			}
			return temp/list.size();
		}
		
		public double getStdDev()
		{
			return Math.sqrt(getVariance());
		}


//		
//		for(int i = 0; i < list.size();i++){
//			Candidate c = list.get(i);
//			c.sigmaScaleProb = (float) (c.sigmaScale/sumSigmaScale);
//		}
		
	
	
	public Population getTopIParents(int number){
		Population result = new Population(rnd_);
		
		for(int i = 0; i < number; i++){
			result.list.add(this.list.get(i));
		}
		
		return result;
	}
	
	public void applyMutationFunction1(double mutationRate){
		for(int i = 0; i < list.size(); i++){
			double change = rnd_.nextDouble();
			if(change >= 0.5){
			for(int j = 0; j <10; j++){
				
					if(list.get(i).genotype[j] + rnd_.nextGaussian()*mutationRate <= 5.0){
						list.get(i).genotype[j] = list.get(i).genotype[j] + rnd_.nextGaussian()*mutationRate;
					}else{
						list.get(i).genotype[j] = list.get(i).genotype[j] - rnd_.nextGaussian()*mutationRate;
					}
						
				}
			}			
		}
	}
	
	
	// TODO: Mutation gaat 1 kant op. Je kan niet de ene keer + en de andere keer -. 
	// Er wordt geen rekening gehouden met de -5. 
	// Bij double change kunnen we ook -1 en 1 kiezen, dan het nieuwe allel berekenen en dan het max pakken met -5 en getal en min van 5 en getal.
	// Change >= 0.5 kan ook een parameter worden, dus niet altijd de helft van de keren. 
	public void applyMutationFunction2and3(double mutationRate){
		for(int i = 0; i < list.size(); i++){
			double change = rnd_.nextDouble();
			if(change >= 0.5){
			for(int j = 0; j <10; j++){
				
					if(list.get(i).genotype[j] + rnd_.nextGaussian()*mutationRate <= 5.0){
						list.get(i).genotype[j] = list.get(i).genotype[j] + rnd_.nextGaussian()*mutationRate;
					}else{
						list.get(i).genotype[j] = list.get(i).genotype[j] - rnd_.nextGaussian()*mutationRate;
					}
						
				}
			}			
		}
	}
	
	
	
	
	
	
	public void applyMutation2(double alpha){
		double thau = alpha/(Math.sqrt(2.0)*Math.sqrt(10.0));
		double thauPrime = alpha/(Math.sqrt(2.0 * 10.0));
		
		for(int i =0; i< list.size();i++){
			ArrayList<Double> tempSigmaList = new ArrayList<Double>();
			Candidate child = list.get(i);
//			System.out.println(child.sigmaList);
//			System.out.println("new sigma list!!!!!!!");
			
			//Het boek zegt dat thauprime voor iedere sigma j anders is. Dus buiten de loop 
			double rnd1 = rnd_.nextGaussian();
			double conThauPrimaRnd = thauPrime*rnd1;
			
			for(int j =0; j<10;j++){
				double rnd22 = rnd_.nextGaussian();
				
//				System.out.println("Old sigma");
//				System.out.println(child.sigmaList.get(j));
				double sigmaPrime = child.sigmaList.get(j) * Math.pow(Math.E, conThauPrimaRnd + thau*rnd22);
				tempSigmaList.add(sigmaPrime);
				
				//kan ook negatief zijn, dus er hoeft maar 1x een waarde berekend te worden. 
				double joe = rnd_.nextGaussian();
				
				double newValue = child.genotype[j] + joe * Math.max(ParameterSettings.MIN_SIGMA_PRIME_0, sigmaPrime);
				
				newValue = Math.max(-5, newValue);
				newValue = Math.min(5,  newValue);
				child.genotype[j] = newValue;
//				System.out.println("New sigma");
//				System.out.println(sigmaPrime);
					
			}
			list.get(i).sigmaList = tempSigmaList;
//			for(int k = 0; k < tempSigmaList.size(); k++){
//				System.out.print(tempSigmaList.get(k));
//				System.out.print(" , ");
//			}
//			System.out.print("\n");
			
			
		}
	}
	
	
}
