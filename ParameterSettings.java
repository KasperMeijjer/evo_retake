public class ParameterSettings {
	
	public static final int SEARCH_SPACE_MIN = -5;
	public static final int SEARCH_SPACE_MAX = 5;
	public static final int POPULATION_SIZE_1 = 100;
	public static final int POPULATION_SIZE_2 = 200;
	public static final int POPULATION_SIZE_3 = 200;

	//hoi
	
	//Function 1
	public static final int NUMBER_OF_PARENTS_IN_SELECTION1 = 50; //(int) (0.7*POPULATION_SIZE);
	public static final double SELECTIOIN_PRESURE = 2;
	
	//Function 2
	public static final int NUMBER_OF_PARENTS_IN_SELECTION2 = 50; //(int) (0.7*POPULATION_SIZE);
	public static final double INITIAL_SIGMA = 5.0;
	public static final double MUTATION_RATE2 = 1.0;//used in applyMutation 
	public static final double ALPHA = 1.0;
	public static final double SIGMA_SCALING = 0.45;
	public static final double SELECTION_PRESURE2 = 1.5;
	public static final double MIN_SIGMA_PRIME_0 = 0.0001;

	//unused for now
	public static final int TOURNEMENT_SIZE = 2;

			
	
	public ParameterSettings(){
		
	}

}
